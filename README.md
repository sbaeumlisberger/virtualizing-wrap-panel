## VirtualizingWrapPanel

Implementation of a VirtualizingWrapPanel and some additional controls based on it for WPF running .NET Framework 4.5+ or .NET Core 3.0+.

### Features
* Horizontal and vertical orientation
* Caching by page, items or pixels
* Container recycling
* Grouping (experimental)
* GridView control (easy to use out of the box experience)
* GridDetailsView control (grid view with item expansion)
* VirtualizingItemsControl

### Resources
* [Sample Application](https://gitlab.com/sbaeumlisberger/virtualizing-wrap-panel/tree/master/downloads)
* [API-Documentation](http://sbaeumlisberger.gitlab.io/virtualizing-wrap-panel/api/WpfToolkit.Controls.html)
* [Source Code](https://gitlab.com/sbaeumlisberger/virtualizing-wrap-panel/tree/master/VirtualizingWrapPanel/VirtualizingWrapPanel)

### Download
* [NuGet Package](https://www.nuget.org/packages/VirtualizingWrapPanel/)
